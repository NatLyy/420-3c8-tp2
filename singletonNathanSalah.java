class Restaurant {
   private static Restaurant dbObject;

   private Restaurant () {      
   }

   public static Restaurant getInstance() {

      // Creation de l objet restaurant
      if(dbObject == null) {
         dbObject = new Restaurant();
      }

       // Sa retourne lobjet restaurant dans ce cas
       return dbObject;
   }

   public void getConnection() {
       System.out.println("Vous etes maintenant rentre dans le restaurant.");
   }
}

class Main {
   public static void main(String[] args) {
      Restaurant db1;

      // On appelle lunique objet restaurant 
      db1= Restaurant.getInstance();

      ///apres execution du code
      // Vous etes maintenant rentre dans le restaurant.

//il y a un seul restaurant , la creation de restaurant autre que lui est impossible
